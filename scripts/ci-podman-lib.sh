#    gitlab-executor-podman
#    Copyright (C) 2021 John Goetz <theodore.goetz@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

function vcheck {
    if [ $# -eq 0 ]; then
        echo "check that a program is at least at the specified version"
        echo "  returns 0 if true"
        echo "usage: vcheck MINVER CMD [OPTS]"
        echo "  [opts] defaults to --version"
        echo "example:"
        echo "  > vcheck 9.2 gcc"
        echo '  > echo $?'
        echo "  0"
        return 1
    fi

    MIN=($(echo "$1" | tr '[:punct:]' '\n')); shift
    CMD="$1"; shift
    if [ $# -gt 0 ]; then
        OPTS="$@"
    else
        OPTS="--version"
    fi

    VER=($("$CMD" "$OPTS" \
           | sed 's/[^0-9._+-]/ /g' \
           | awk '{print $1}' \
           | tr '[:punct:]' '\n'))

    for i in `seq 0 ${#MIN[@]}`; do
        if [[ ${#VER[@]} -ge $i ]]; then
            if [[ ${VER[$i]} -gt ${MIN[$i]} ]]; then
                break
            elif [[ ${VER[$i]} -lt ${MIN[$i]} ]]; then
                MINVER=`tr ' ' '.' <<< "${MIN[@]}"`
                CMDVER=`tr ' ' '.' <<< "${VER[@]}"`
                echo "ERROR: $CMD version $CMDVER is too old."
                echo "       Minimum required version: $MINVER"
                return 1
            fi
        fi
    done
    return 0
}

function install-command {
    CONTAINER_NAME="$1"
    CMD="$(basename $2)"

    podman exec --interactive "$CONTAINER_NAME" $CMD <<< "true" && return

    echo "attempting to install $CMD using one of: apk apt-get dnf yum zypper"
    EXEC=(podman exec --user root:root --interactive "$CONTAINER_NAME" sh -c)
    ${EXEC[@]} "type apk &>/dev/null && apk add $CMD" && return
    ${EXEC[@]} "type apt-get &>/dev/null && apt-get install -y --no-install-recommends ca-certificates $CMD" && return
    ${EXEC[@]} "type dnf &>/dev/null && dnf install -y --setopt=install_weak_deps=False $CMD" && return
    ${EXEC[@]} "type pacman &>/dev/null && pacman -Sy $CMD" && return
    ${EXEC[@]} "type yum &>/dev/null && yum install -y $CMD" && return
    ${EXEC[@]} "type zypper &>/dev/null && zypper install -y --no-recommends $CMD" && return
}
